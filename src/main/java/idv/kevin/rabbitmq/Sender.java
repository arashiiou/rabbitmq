package idv.kevin.rabbitmq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;


@Component
public class Sender{

	private final RabbitTemplate rabbitTemplate;

	public Sender(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}

	// 發送消息
	public void send(String msg) {
		System.out.println("Sender : " + msg);

		this.rabbitTemplate.convertAndSend("RabbitMQ Test", msg);
	}
}

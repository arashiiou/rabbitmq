package idv.kevin.rabbitmq;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	private Sender sender;

	public Controller(Sender sender) {
		this.sender = sender;
	}

	@GetMapping(value = "/send")
	public String send(String msg){
		sender.send(msg);
		return "消息：" + msg + ",已發送";
	}
}

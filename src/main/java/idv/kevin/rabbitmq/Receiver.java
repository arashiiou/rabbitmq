package idv.kevin.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@RabbitListener(queues = "RabbitMQ Test")
public class Receiver {


	// 接收到消息的處理方法
	@RabbitHandler
	public void process(String message) {
		System.out.println("Receiver : " + message);
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

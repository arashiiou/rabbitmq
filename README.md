1. create rabbitMQ service
```    
$ brew install rabbitmq
```
then
```
$ rabbitmq-server
```

management pag http://localhost:15672

default username: guest <br>
default password: guest

default service port: 5672


access localhost:8080/send?msg=xxxxx
('xxxx' is what you want to send)
